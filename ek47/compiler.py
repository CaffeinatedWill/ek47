import os
import shutil
import subprocess
import ek47

from ek47 import its
from ek47.output import colorize, display
from ek47.generate import generate_string_identifier, generate_snk_file

#ToDo: We need stderr output for all functions like compiler has


def check_mcs_installed() -> bool:
    """
    Check if the Mono C# Compiler (`mcs`) is already installed.

    This function attempts to run the `mcs --version` command to check if the Mono C# Compiler is installed
    on the system. It captures the output and checks for errors to determine if `mcs` is installed or not.

    Returns:
        bool: True if `mcs` is installed, False otherwise.
    """
    subprocess.run(['mcs', '--version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    return True


def check_mono_complete_installed() -> bool:
    """
    Check if the Mono C# Compiler (`mcs`) is already installed.

    This function attempts to run the `mcs --version` command to check if the Mono C# Compiler is installed
    on the system. It captures the output and checks for errors to determine if `mcs` is installed or not.

    Returns:
        bool: True if `mcs` is installed, False otherwise.
    """
    subprocess.run(['mono', f'{os.getcwd()}/resources/nuget.exe'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    return True


def compiler(input_file: str, output_file: str, compiler_arguments: list, sign: bool = False, unsafe: bool = False, hidewindow: bool = False) -> bool:
    # '{mcs} -w:1 -out:{out} -unsafe{unsafe} -target:{target} {reference} {sign} -debug- -optimize+ {cs_file}')
    unsafe = '+' if unsafe else '-'

    # -target. Valid options are `exe', `winexe', `library' or `module'
    # module should never be used.
    # if there is already a target specified, don't overwrite it.
    if hidewindow:
        if any('-target' in argument for argument in compiler_arguments):
            display(f"--hidewindow selected but a -target is already explicitly provided",
                    'INFORMATION')
        else:
            compiler_arguments.append('-target:winexe')

    if sign:
        snk_file = f'{os.getcwd()}/{generate_string_identifier()}.snk'
        generate_snk_file(snk_file)
        sign = f"-keyfile:{snk_file}"
        display(f"Signing assembly with SNK file: {snk_file}. Verify signature with `sn -v {output_file}`",
                'INFORMATION')
    else:
        sign = ''

    compiler_arguments = [ 
        shutil.which('mcs'),
        '-w:1', 
        '-debug-', 
        '-optimize+', 
        f'-unsafe{unsafe}', 
        sign,
        *compiler_arguments, 
        f'-out:{output_file}', 
        input_file
    ]
    display(f'Attempting to compile `{input_file}`: {" ".join(compiler_arguments)}', debug=ek47.DEBUG)
    try:
        subprocess.run(compiler_arguments, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    except subprocess.CalledProcessError as error:
        for error_line in error.stderr.decode('utf-8').split('\n'):
            if not error_line:
                continue
            display(colorize(error_line, 'red', bold=True), 'ERROR')
        return False

    display(f'Successfully compiled `{input_file}` to `{output_file}`.', 'SUCCESS')
    if sign:
        display(f'Removing {snk_file} as we no longer need it.', debug=ek47.DEBUG)
        os.remove(snk_file)
    return True


def install_dependencies(dependencies: list):
    """
    Install NuGet dependencies.

    This function attempts to install a list of NuGet dependencies using the `nuget install` command.
    It relies on the `check_nuget_installed` function to check if `nuget` is already installed.

    Args:
        dependencies (list): List of NuGet packages to install.

    Returns:
        bool: True if installation is successful, False otherwise.
    """
    if not check_mono_complete_installed():
        display(colorize('Attempted to install dependencies when `mono-complete` is not installed.', 'red', bold=True), 'ERROR')
        return False
    display(f"Attempting to install dependencie(s) (`{colorize(' '.join(dependencies), 'white', bold=True)}`) via {colorize('nuget', 'white', bold=True)}", 'INFORMATION')
    
    arguments = [
        'mono',
        f'{os.getcwd()}/resources/nuget.exe',
        'install',
        ' '.join(dependencies)
    ]
    subprocess.run(arguments, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    display(f"Successfully installed dependencies(s): {' '.join(dependencies)}", 'SUCCESS')
    return True

def install_mcs():
    """
    Installs the Mono C# compiler (`mcs`) if it is not already installed.
    
    This function checks if the Mono C# compiler is already installed on the system. If it is,
    the function returns without taking any action. If the compiler is not found, the function
    attempts to install it using the package manager (apt or brew) based on the system's availability.
    
    If the installation is successful, a success message is displayed; otherwise, an error message
    is shown, suggesting the user install `mcs` manually.
    """
    if check_mcs_installed():
        display(f"`mcs` is already installed skipping automatic installation", 'INFORMATION')
        return
    if its.linux:
        display(f"Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('apt', 'white', bold=True)}", 'INFORMATION')
        subprocess.run(f"{shutil.which('apt')}", 'install', 'mono-complete', stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        return
    if its.mac:
        display(f"Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('brew', 'white', bold=True)}", 'INFORMATION')
        subprocess.run(f"{shutil.which('brew')}", 'install', 'mono', stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        return
    if check_mcs_installed():
        display(colorize('`mcs` sucesfully installed!', 'green', bold=True), 'SUCCESS')
    else:
        display(colorize('`mcs` could not be automatically installed! You might have to install it manully!', 'red', bold=True), 'ERROR')
      
def install_mono_complete():
    """
    Installs the Mono C# compiler (`mono-complete`) if it is not already installed.

    This function checks if the Mono is already installed on the system. If it is,
    the function returns without taking any action. If the compiler is not found, the function
    attempts to install it using the package manager (apt or brew) based on the system's availability.

    If the installation is successful, a success message is displayed; otherwise, an error message
    is shown, suggesting the user install `mono-complete` manually.
    """
    if check_mono_complete_installed():
        display(f"`mono-complete` is already installed skipping automatic installation", 'INFORMATION')
        return
    if its.linux:
        display(f"Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('apt', 'white', bold=True)}", 'INFORMATION')
        subprocess.run(f"{shutil.which('apt')}", 'install', 'mono-complete', stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        return
    if its.mac:
        display(f"Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('brew', 'white', bold=True)}", 'INFORMATION')
        subprocess.run(f"{shutil.which('brew')}", 'install', 'mono', stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        return
    if check_mono_complete_installed():
        display(colorize('`mono-complete` sucesfully installed!', 'green', bold=True), 'SUCCESS')
    else:
        display(colorize('`mono-complete` could not be automatically installed! You might have to install it manully!', 'red', bold=True), 'ERROR')
