from . import bof, dotnet, dinvoke_shellcode, shellcode, unmanaged_dll

bof = bof.Bof()
dinvoke_shellcode = dinvoke_shellcode.DinvokeShellcode()
dotnet = dotnet.Dotnet()
shellcode = shellcode.Shellcode()
unmanaged_dll = unmanaged_dll.UnmanagedDll()

SUPPORTED_INPUT_PAYLOAD_FORMATS = {
    bof.name:bof,
    dinvoke_shellcode.name:dinvoke_shellcode,
    dotnet.name:dotnet,
    shellcode.name:shellcode,
    unmanaged_dll.name:unmanaged_dll
}
