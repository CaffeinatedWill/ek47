from . import clickonce, dotnet, install_util, msbuild, regasm, service_exe

clickonce = clickonce.ClickOnce()
dotnet = dotnet.DotNet()
install_util = install_util.InstallUtil()
msbuild = msbuild.MSBuild()
regasm = regasm.Regasm()
service_exe = service_exe.ServiceExe()

SUPPORTED_OUTPUT_PAYLOAD_FORMATS = {
    clickonce.name: clickonce,
    dotnet.name: dotnet,
    install_util.name: install_util,
    msbuild.name: msbuild,
    regasm.name: regasm,
    service_exe.name: service_exe
}
