import datetime
import os
import textwrap

import pefile
import random
import ek47

from ek47.generate import generate_random_business_words
from ek47.output import colorize, display
from ek47.util import calculate_shannon_entropy
from jinja2 import Environment, FileSystemLoader


class OutputFormatBase:
    TEMPLATES_DIRECTORY = f'{os.getcwd()}/templates/outputs/'
    JINJA2_ENVIRONMENT = Environment(loader=FileSystemLoader(TEMPLATES_DIRECTORY))

    def __init__(self, template_name, name: str, description: str):
        self.description = description
        self.name = name
        self.template_name = template_name

    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        display(f'Compiler is enabled but the {self.name} output format does not support compilation.', 'INFORMATION')
        return '', False

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        raise NotImplementedError(f"The {self.name} payload output format has not implemented render_template.")

    def generate(self, arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name) -> str:
        if input_format_name == 'dotnet':
            arguments.method = arguments.method
            if arguments.payload_args:
                self.setup_payload_arguments(arguments)
        else:
            arguments.method = 'Main'
        display(f'Attempting to render `{self.template_name}`', debug=ek47.DEBUG)
        rendered_template = self.render_template(arguments, encrypted_payload, encrypted_bypass, environmental_keys)
        display(f'Successfully rendered template, `{self.template_name}`', 'SUCCESS')
        rendered_template_file = f'{arguments.output}.cs'
        with open(rendered_template_file, 'w') as fd:
            fd.write(rendered_template)
            display(f"Writing {len(rendered_template):,} byte template to `{rendered_template_file}`.",
                    'SUCCESS')
        output_file, compiled = self.compile_template(arguments, rendered_template_file, compiler)
        if not compiled:
            display(f'Did not compile the rendered template, skipping entropy check.', 'INFORMATION')
            return
        display(f'Fixing time date stamp.', debug=ek47.DEBUG)
        self.write_time_date_stamp(output_file)
        with open(output_file, 'rb') as fd:
            entropy = calculate_shannon_entropy(fd.read())
            color = "green" if 4.5 <= entropy < 5.5 else "red"
            display(f'Shannon entropy: {colorize(str(entropy), color, bold=True)} / 8.000', 'INFORMATION')
        return output_file

    def make_c_sharp_byte_array(self, bytes_input: bytes, array_name: str, no_entropy: bool = False) -> str:
        # Create a C# byte array representation of the bytes_input
        # passed to this function.

        # Just generate a simple byte array
        if no_entropy:
            return f"{array_name}.Add(new byte[] {self.make_simple_c_sharp_byte_array(bytes_input)});"

        newline_counter = 1
        c_sharp_byte_array = f"{array_name}.Add(new byte[] {{"

        for byte in bytes_input:
            c_sharp_byte_array += hex(byte) + ","
            if newline_counter > 20:
                newline_counter = 0
                c_sharp_byte_array += "});\n" + " " * 12
                if random.randint(1, 3) == 1:  # Roll a 3 sided die
                    c_sharp_byte_array += f'test = "{generate_random_business_words()}";'
                    c_sharp_byte_array += "\n" + " " * 12 + f"{array_name}.Add(new byte[] {{"
                else:
                    c_sharp_byte_array += f"{array_name}.Add(new byte[] {{"
            newline_counter += 1

        return c_sharp_byte_array + "});"

    @staticmethod
    def make_c_sharp_list(pylist: list) -> str:
        # Take a Python list and format it without the brackets
        # We also need to reverse the list, since we decrypt in the
        # reverse order we encrypted in (thx Skyler)
        result = ""
        pylist.reverse()
        for item in pylist:
            result = result + '"' + str(item) + '",'
        return result

    @staticmethod
    def make_simple_c_sharp_byte_array(bytes_input: bytes, reverse: bool = False) -> str:
        # Makes a really simple byte array
        if reverse:
            bytes_input = bytes_input[::-1]
        c_sharp_byte_array = f"{{"
        for byte in bytes_input:
            c_sharp_byte_array += hex(byte) + ","
        c_sharp_byte_array += f"}}"
        return c_sharp_byte_array

    @staticmethod
    def read_time_date_stamp(filename: str) -> str:
        # Given a file name, returns the PE TimeDateStamp date in UTC representation
        pe = pefile.PE(filename)
        ts = int(pe.FILE_HEADER.dump_dict()['TimeDateStamp']['Value'].split()[0], 16)
        utc_time = datetime.datetime.fromtimestamp(ts)
        return utc_time.strftime("%Y-%m-%d %H:%M:%S")

    def write_time_date_stamp(self, filename: str) -> None:
        # Replace the TimeDateStamp compile time in a given file
        compile_date = self.generate_random_date()
        pe = pefile.PE(filename)
        timestamp = int(compile_date.timestamp())
        pe.FILE_HEADER.TimeDateStamp = timestamp
        pe.write(filename)
        display(f"Updated PE compile date to: {self.read_time_date_stamp(filename)}", 'INFORMATION')

    @staticmethod
    def generate_random_date(days_ago: int = 180, jitter: float = 0.75) -> datetime:
        # Generates a DateTime in the past that is suitable for a compile time
        seconds = days_ago * 24 * 60 * 60 # 24 * 60 * 60 = seconds in a day
        jitter = round(jitter * seconds)
        timestamp = random.randint(seconds-jitter, seconds+jitter)
        ago = datetime.timedelta(seconds=timestamp)
        return datetime.datetime.now() - ago

    @staticmethod
    def setup_payload_arguments(arguments):
        if os.path.isfile(arguments.payload_args[0]):
            with open(arguments.payload_args[0], "r") as fd:
                payload_arguments = fd.read().rstrip()
                display(f'Hardcoded arguments into loader: {payload_arguments}', 'INFORMATION')
                arguments.payload_args = f'args = new string[] {{{ payload_arguments }}};'
                return
        payload_arguments_string = 'args = new string[] { '
        for argument in arguments.payload_args:
            argument = argument.replace('\\', '\\\\')[::-1]
            payload_arguments_string += '"' + argument + '"' + ','
        payload_arguments_string += '};'
        payload_arguments_string += textwrap.dedent("""
        int index = 0;
        foreach(string arg in args)
        {
            args[index] = string.Join("", arg.ToCharArray().Reverse().ToArray());
            index++;
        }
        """)
        display(f'Hardcoded arguments into loader: {arguments.payload_args}', 'INFORMATION')
        arguments.payload_args = payload_arguments_string


class Ek47EmbeddedFormat(OutputFormatBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate_payload(self, arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name):
        output_file = self.generate(arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name)
        self.embed_ek47(arguments, output_file)

    def embed_ek47(self, arguments, output_file):
        raise NotImplementedError(f"The {self.name} payload output format has not implemented embed_ek47.")


class CompiledFormat(OutputFormatBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate_payload(self, arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name):
        self.generate(arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name)

