import json
import math
import os
import random
import re

from .output import colorize, display
from urllib.parse import urlparse

def check_naughty_filename(filename: str) -> bool:
    # Implement checks such that the output filename is not a DIRTY WORD
    # If you really want to bypass this, just uncomment the line below. Hack responsibly

    # return True

    naughty_words = ["sploit", "meterpreter", "msf", "metsrv", "stage",
                     "badrat", "ek47", "cobalt", "beacon", "potato",
                     "shell", "malware", "payload", "privesc", "grunt",
                     "implant", "exploit", "rubeus", "bloodhound",
                     "mimikatz", "sharp", "dump", "sliver", "empire"]

    REEEEEEEEEE = re.compile('|'.join(naughty_words), re.IGNORECASE)
    if(REEEEEEEEEE.match(filename)):
        display(colorize(
            f"You are hereby found guilty of generating a payload with a naughty name. Your payload was named: {filename}",
            'red', bold=True), 'ERROR')
        display(colorize(f"Your punishment for this crime is ", 'red', bold=True) + colorize(
            f"{random.randint(5, 25)} lashes! ", 'white', bold=True) + colorize(
            f"Please lash yourself at your earliest convenience.", 'red', bold=True), 'ERROR')
        display(colorize(f"Tip: Use the \"--output\" flag to change the output payload name.", 'white', bold=True), 'ERROR')
        return False
    return True

def calculate_shannon_entropy(data: bytes) -> float:
    """
    Calculate the Shannon entropy value of the given data.

    The Shannon entropy measures the amount of uncertainty or information contained in a set of data.
    The result will be between 0.000 and 8.000.

    Args:
        data (bytes): The input data for entropy calculation.

    Returns:
        float: The calculated Shannon entropy value.

    References:
        - Original code: https://github.com/stanislavkozlovski/python_exercises/blob/master/easy_263_calculate_shannon_entropy.py
        - Stack Overflow: https://stackoverflow.com/questions/6256437/entropy-in-binary-files-whats-the-purpose
    """
    probability = [float(data.count(c)) / len(data) for c in dict.fromkeys(list(data))]
    entropy = -sum([p * math.log(p) / math.log(2.0) for p in probability])
    return round(entropy, 3)

def convert_environmental_keys_arguments_to_dict(arguments):
    env = {}
    if arguments.json:
        try:
            env = json.loads(arguments.json)
            display(f"Adding {len(env)} items to key on from JSON data", 'INFORMATION')
        except:
            display("Invalid JSON string. (Did you put 'single quotes' around your JSON string?) Exiting!", 'ERROR')
            quit()
    if arguments.username:
        env['username'] = arguments.username.upper()
        display(f"Adding username to list of items to key on: {env['username']}", 'INFORMATION')
    if arguments.domain:
        env['userdomain'] = arguments.domain.upper()
        if "." in env['userdomain']:
            display(colorize(f"Domain name ({env['userdomain']}) should be specified as a NETBIOS-style domain name. This probably won't work! (Try \"{env['userdomain'].split('.')[0]}\" instead).", color="red", bold=True), 'WARN')
        display(f"Adding domain to list of items to key on: {env['userdomain']}", 'INFORMATION')
    if arguments.computername:
        env['computername'] = arguments.computername.upper()
        if "." in env['computername']:
            display(colorize(f"Computer name ({env['computername']}) should not be specified as an FQDN or have dots (\".\") in it. This probably won't work! (Try \"{env['computername'].split('.')[0]}\" instead).", color="red", bold=True), 'WARN')
        display(f"Adding computername to list of items to key on: {env['computername']}", 'INFORMATION')
    if arguments.guardwords:
        arguments.guardwords.reverse()
        param_str = ""
        for i, guardword in enumerate(arguments.guardwords, start=1):
            env[f'guardword{i}'] = guardword
            param_str = f"{guardword} {param_str}"
        display(f"Adding guardword(s) to list of items to key on. First argument(s) to executable must be: {param_str.strip()}", 'INFORMATION')
    return env

def filter_arguments(arguments):
    """
    Filter and validate the command-line arguments.

    Args:
        arguments (argparse.Namespace): The parsed command-line arguments.

    Raises:
        ValueError: If conflicting or invalid arguments are detected.

    Returns:
        argparse.Namespace: The filtered and validated command-line arguments.
    """

    if not any([arguments.username, arguments.domain, arguments.computername, arguments.guardwords, arguments.json]):
        display(colorize("At least one of --username (-u), --domain (-d), --computername (-c), --guardwords (-g), or "
                         "--json (-j) must be specified.", 'red', bold=True), 'ERROR')
        return False

    if arguments.forceexecute and arguments.bypass.lower() == "none":
        display(colorize("Cannot use --force-execute (-f) option at the same time as --bypass none.", 'red', bold=True),
                'ERROR')
        return False

    mutually_exclusive_options = [arguments.service_exe, arguments.install_util, arguments.msbuild, arguments.clickonce,
                                  arguments.regasm]
    if mutually_exclusive_options.count(True) > 1:
        display(colorize(
            "The --service-exe, --install-util, --msbuild, --clickonce, and --regasm options are mutually exclusive! Choose only one.",
            'red', bold=True), 'ERROR')
        return False

    if arguments.clickonce:
        try:
            result = urlparse(arguments.deployment_url)
            if not all([result.scheme, result.netloc]):
                display(colorize(f'`{arguments.deployment_url}` is not a valid URL!', 'red', bold=True), 'ERROR')
                return False
        except:
            display(colorize(f'`{arguments.deployment_url}` is not a valid URL!', 'red', bold=True), 'ERROR')
            return False
        if not arguments.deployment_url.endswith(".application"):
            display(colorize("Clickonce payloads' deployment URL must end with \".application\"!", 'red', bold=True),
                    'ERROR')
            return False

    return arguments

def resolve_output(arguments) -> str:
    """
    Resolve the output file path based on the provided arguments and output.

    Args:
        arguments (object): The object containing the parsed command-line arguments.

    Returns:
        str: The resolved output file path
    """

    output = arguments.output

    # No output given. Derive the name of the output file from the payload name.
    if not output:
        payloadname = os.path.basename(arguments.payload)
        payloadname_without_extension = os.path.splitext(payloadname)[0]
        return f'{payloadname_without_extension}_ek'

    # output is a directory. Derive the file name from the payload and join that with the directory path.
    if os.path.isdir(output):
        path = output
        payloadname = os.path.basename(arguments.payload)
        payloadname_without_extension = os.path.splitext(payloadname)[0]
        return os.path.join(path, f'{payloadname_without_extension}_ek')

    # output is a filename. Remove the extension from the output file name. We will add our own later.
    return os.path.splitext(output)[0]
