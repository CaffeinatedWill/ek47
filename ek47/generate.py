import os
import random
import struct

from Crypto.PublicKey import RSA
from Crypto.Util.number import long_to_bytes
from io import BytesIO

alphabet = []
business_words = []

with open(f'{os.getcwd()}/resources/alphabet.txt', 'r') as _alphabet:
    for character in _alphabet:
        alphabet.append(character.rstrip())

with open(f'{os.getcwd()}/resources/business_words.txt', 'r') as _business_words:
    for word in _business_words:
        business_words.append(word.rstrip())


def generate_random_business_words(min_length: int = 5, max_length: int = 60) -> str:
    """
    Generate a random sentence from the given word list with a specified length.

    Args:
        min_length (int, optional): Minimum length of the sentence (number of words). Defaults to 5.
        max_length (int, optional): Maximum length of the sentence (number of words). Defaults to 60.

    Returns:
        str: A random sentence.
    """
    sentence_length = random.randint(min_length, max_length)
    sentence = " ".join(random.choices(business_words, k=sentence_length))
    sentence = sentence.capitalize() + "."
    return sentence


def generate_string_identifier(length: int = 10) -> str:
    """
    Generate random upper and lowercase characters and concatenate
    them for a length of zero to *length*.
    :param: int length: The amount of random characters to concatenate.
    :return: The generated string identifier.
    :rtype: str
    """
    _identifier = [alphabet[random.randint(0, len(alphabet) - 1)].rstrip() for _ in range(0, length)]
    _identifier = ''.join(_identifier)
    return _identifier


def generate_snk_file(filename: str, key_size: int = 1024) -> None:
    # Generates a self-signed Strong Name Key file which can be used with
    # the `mcs` compiler's -keyfile: flag to sign an assembly with a "strong name".
    # Equivalent to the `sn -k MyKey.snk` command.
    # Signing an assembly is required for regsvcs.exe and regasm.exe AWL bypasses.
    # Reference: https://github.com/AppCoreNet/SigningTool/blob/dac2e64c2913e79bc96016a3befe1891be1f21fa/src/AppCore.SigningTool/StrongName/StrongNameKeyGenerator.cs#L19-L41
    rsa = RSA.generate(key_size)
    RSA2_SIG = 0x32415352

    parameters = rsa.export_key()
    e = long_to_bytes(rsa.e)
    n = long_to_bytes(rsa.n)
    p = long_to_bytes(rsa.p)
    q = long_to_bytes(rsa.q)
    d = long_to_bytes(rsa.d)
    dp = long_to_bytes(rsa.d % (rsa.p-1))
    dq = long_to_bytes(rsa.d % (rsa.q-1))
    qInv = long_to_bytes(pow(rsa.q, rsa.p-2, rsa.p))

    out_stream = BytesIO()
    writer = BytesIO()
    writer.write(struct.pack('B', 7))  # bType (public/private key)
    writer.write(struct.pack('B', 2))  # bVersion
    writer.write(struct.pack('H', 0))  # reserved
    writer.write(struct.pack('I', 0x00002400))  # aiKeyAlg (CALG_RSA_SIGN)
    writer.write(struct.pack('I', RSA2_SIG))  # magic (RSA2)
    writer.write(struct.pack('I', len(n)*8))  # bitlen
    writer.write(e[::-1])  # exponent
    writer.write(b'\x00' * (4 - len(e) % 4))  # pad to DWORD
    writer.write(n[::-1])  # modulus
    writer.write(p[::-1])  # prime1
    writer.write(q[::-1])  # prime2
    writer.write(dp[::-1])  # exponent1
    writer.write(dq[::-1])  # exponent2
    writer.write(qInv[::-1])  # coefficient
    writer.write(d[::-1])  # privateExponent
    out_stream.write(writer.getvalue())

    snk_data = out_stream.getvalue()
    with open(filename, "wb") as f:
        f.write(snk_data)